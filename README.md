###  Beacon Network v2 Java implementation

###### Jakarta EE Platform 9.1
The implementation is based on Jakarta RESTful Web Services 3.0 API ([JAX-RS 3.0](https://jakarta.ee/specifications/restful-ws/3.0/))
The Beacon Network Aggregator is developed and deployed on [WildFly 27.0.1](http://wildfly.org/) server.

###### Beacon v2 Java implementation
The Aggregator is based on [Beacon v2 Java](https://gitlab.bsc.es/inb/ga4gh/beacon-v2-api) library.

###### Apache Maven build system
To simplify build process API uses [Apache Maven](https://maven.apache.org/) build system.

To build OpenEBench REST application:

Compiling:
```shell
>git clone https://gitlab.bsc.es/inb/ga4gh/beacon-network-v2.git
>cd beacon-network-v2
>mvn install
```
This must create `beacon-network-v2-x.x.x.war` (**W**eb application **AR**chive) application in the `/target` directory.

###### BSC Maven Repository
There is no need to build the project manually, as it uses "Continuous Building" and it is possible to download the build from:  
https://inb.bsc.es/maven/es/bsc/inb/ga4gh/beacon-network-v2-ui/0.0.2/beacon-network-v2-ui-0.0.2.war

###### WildFly server
WildFly is a free opensource JEE server and may be easy downloaded from it's website: (http://wildfly.org/).
The deployment is as simple as dropping the compiled `beacon-network-v2-x.x.x.war` application in the `$WILDFLY_HOME/standalone/deployments/`
directory and run the application server `$WILDFLY_HOME/bin/standalone.sh`.

###### Configuration
Current implementation doesn't provide many options for configuration.
There are three configuration files in the `/BEACON-INF` directory:
* `configuration.json` - standard beacon configuration file: [beaconConfigurationResponse.json](https://github.com/ga4gh-beacon/beacon-v2/blob/main/framework/json/responses/beaconConfigurationResponse.json)
* `beacon-info.json` - standard beacon information file: [beaconInfoResponse.json](https://github.com/ga4gh-beacon/beacon-v2/blob/main/framework/json/responses/beaconInfoResponse.json)
* `beacon-network.json` - Json Array of backed Beacons' endpoints  

The example of the `beacon-network.json`:
```json
[
  "https://beacons.bsc.es/beacon/v2.0.0",
  "https://ega-archive.org/test-beacon-apis/cineca"
]
```
Note that the **W**eb application **AR**chive (WAR) is just a usual ZIP file so one can edit these configurations manually without the need to rebuild the application.

It is also possible to define external directory for the `beacon-network.json` configuration.
```bash
export BEACON_NETWORK_CONFIG_DIR=/wildfly/BEACON-INF
```
When the `BEACON_NETWORK_CONFIG_DIR` is set, the aggregator monitors the `$BEACON_NETWORK_CONFIG_DIR/beacon-network.json` to dynamically update the configuration.

###### Error Messages
During the initialization and work the application may write errors to the server log console.
Pay an attention to the SEVERE messages as they can report errors in the beacons configurations:
```shell
14:44:05,539 SEVERE [es.bsc.inb.ga4gh.beacon.network.config.NetworkConfiguration] (Weld Thread Pool -- 1) error loading from https://ega-archive.
org/test-beacon-apis/cineca/filtering_terms Server returned HTTP response code: 503 for URL: https://ega-archive.org/test-beacon-apis/cineca/filtering_terms
```
